import UIKit

class ViewController: UIViewController {
  let data = ["1", "2", "3", "4", "5"]
  let followersLabel: UILabel = {
    let vw = UILabel()
    vw.text = "followers".uppercased()
    vw.textColor = UIColor(red: 0.36, green: 0.42, blue: 0.50, alpha: 1.0)
    vw.frame = .zero
    vw.translatesAutoresizingMaskIntoConstraints = false
    return vw
  }()
  let headerView: UIView = {
    let vw = UIView()
    vw.frame = .zero
    vw.backgroundColor = BG_COLOR
    vw.translatesAutoresizingMaskIntoConstraints = false
    return vw
  }()
  let tableView: UITableView = {
    let tw = UITableView.init(frame: .zero, style: UITableView.Style.plain)
    tw.backgroundColor = BG_COLOR
    tw.translatesAutoresizingMaskIntoConstraints = false
    tw.rowHeight = 60
    tw.tableFooterView = UIView(frame: .zero)
    return tw
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.addSubview(tableView)
    
    self.tableView.register(ChartListCell.self, forCellReuseIdentifier: "TableViewCell")
    self.tableView.dataSource = self
    self.tableView.delegate = self
    loadConstraints()
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.navigationBar.barTintColor = VIEW_COLOR
    self.navigationController?.navigationBar.isTranslucent = false
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    self.navigationItem.title = "Statistic"
    view.backgroundColor = BG_COLOR
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch tableView {
    case self.tableView:
      return self.data.count
    default:
      return 0
    }
  }

}

extension ViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! ChartListCell
    cell.textLabel?.text = self.data[indexPath.row]
    return cell
  }
  // Table Header
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = UIView()
    header.addSubview(followersLabel)
    header.heightAnchor.constraint(equalToConstant: 60).isActive = true
    followersLabel.bottomAnchor.constraint(equalTo: header.bottomAnchor, constant: -5).isActive = true
    followersLabel.leadingAnchor.constraint(equalTo: header.leadingAnchor, constant: 20).isActive = true
    return header
  }
  
}

extension ViewController: UITableViewDelegate {
  
}

extension ViewController {
  func loadConstraints() {
    tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
    tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
    tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    
  }
}

