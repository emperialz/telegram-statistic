//
//  Constants.swift
//  tgstatistic
//
//  Created by Дмитрий on 12/03/2019.
//  Copyright © 2019 Supreme. All rights reserved.
//

import Foundation
import UIKit

let BG_COLOR = UIColor(red: 0.09, green: 0.13, blue: 0.18, alpha: 1.0)
let VIEW_COLOR = UIColor(red: 0.13, green: 0.19, blue: 0.25, alpha: 1.0)
